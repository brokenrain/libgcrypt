/* Define to the version of this package. */
#define PACKAGE_VERSION "1.9.2"

/* Version of this package */
#define VERSION "1.9.2"

/* GIT commit id revision used to build this package */
#define BUILD_REVISION "24bd7e8"

/* The time this package was configured for a build */
#define BUILD_TIMESTAMP "2021-02-25T16:13+0000"
